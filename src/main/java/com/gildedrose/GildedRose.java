package com.gildedrose;

import com.gildedrose.core.GildedRoseCore;
import com.gildedrose.items.IdItem;

import java.util.List;
import java.util.stream.IntStream;

import static com.gildedrose.core.AsyncUtil.runWithLoggingAndSlowdown;
import static java.util.stream.Collectors.toList;

/**
 * See {@link com.gildedrose.core.GildedRoseCore} for the core quality/sellIn computation code.
 * <p>
 * See the history of this file for full refactoring picture.
 */
class GildedRose {

    Item[] items;
    private boolean asyncSlowdown = false;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void setAsyncSlowdown(boolean slowdown) {
        this.asyncSlowdown = slowdown;
    }

    public void updateQuality() {
        getAllItems().stream()
            // run async (could have used CompletableFuture, but result is basically the same with defaults)
            .parallel()
            .map(this::updateItemQualityAndSellIn)
            .forEach(this::updateItemFromResult);
    }

    private synchronized List<IdItem> getAllItems() {
        return IntStream.range(0, items.length)
            .mapToObj(index -> new IdItem(index, items[index]))
            .collect(toList());
    }

    private IdItem updateItemQualityAndSellIn(IdItem idItem) {
        return runWithLoggingAndSlowdown(asyncSlowdown,
            () -> GildedRoseCore.updateItemQualityAndSellIn(idItem));
    }

    private synchronized void updateItemFromResult(IdItem updatedIdItem) {
        // cast to int, don't expect ids exceeding ints
        Item actualItem = items[(int) updatedIdItem.id];
        updatedIdItem.updateValuesIn(actualItem);
    }

}
