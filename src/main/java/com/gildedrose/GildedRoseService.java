package com.gildedrose;

import com.gildedrose.core.GildedRoseCore;
import com.gildedrose.items.IdItem;
import com.gildedrose.items.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static com.gildedrose.core.AsyncUtil.runWithLoggingAndSlowdown;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

@Service
public class GildedRoseService {

    private static final Logger LOG = LoggerFactory.getLogger(GildedRoseService.class);

    private static final List<Item> DEFAULT_ITEMS = Collections.unmodifiableList(Arrays.asList(
        new Item("+5 Dexterity Vest", 10, 20),
        new Item("Aged Brie", 2, 0),
        new Item("Elixir of the Mongoose", 5, 7),
        new Item("Sulfuras, Hand of Ragnaros", 0, 80),
        new Item("Sulfuras, Hand of Ragnaros", -1, 80),
        new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
        new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
        new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
        new Item("Conjured Mana Cake", 3, 6)
    ));

    private final AtomicLong idGenerator = new AtomicLong();

    private final ItemRepository itemRepository;

    private AtomicBoolean updating = new AtomicBoolean(false);

    @Autowired
    public GildedRoseService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public void updateAllAsync() {
        // 'lock' update process to avoid two processes running at the same time
        lockUpdates();

        LOG.info("Updating all items");

        // process stream elements in parallel by using CompletableFutures
        Iterable<IdItem> allItems = itemRepository.findAll();
        CompletableFuture[] allUpdateFutures =
            stream(allItems.spliterator(), false)
                .map(this::slowUpdateItemQualityAndSellIn)
                .map(future -> future.thenAccept(this::saveUpdatedItem))
                .toArray(CompletableFuture[]::new);

        // when all complete asynchronously, reset the updating flag
        CompletableFuture.allOf(allUpdateFutures)
            .whenComplete(((aVoid, throwable) -> unlockUpdates()));
    }

    private CompletableFuture<IdItem> slowUpdateItemQualityAndSellIn(IdItem idItem) {
        // artificially slowdown updates
        return CompletableFuture.supplyAsync(() ->
            runWithLoggingAndSlowdown(true,
                () -> GildedRoseCore.updateItemQualityAndSellIn(idItem)));
    }

    private void saveUpdatedItem(IdItem updatedItem) {
        // cast to int, don't expect ids exceeding ints
        itemRepository.save(updatedItem);
    }

    public void resetAllItems() {
        lockUpdates();

        LOG.info("Resetting all items - default items will be inserted");
        itemRepository.deleteAll();
        insertDefaultItems();

        unlockUpdates();
    }

    public List<IdItem> getAllItems() {
        List<IdItem> allItems = loadAllItems();

        if (!allItems.isEmpty()) {
            return allItems;
        }

        LOG.info("Inserting default items into empty repository");
        return insertDefaultItems();
    }

    private List<IdItem> loadAllItems() {
        LOG.info("Loading all items");
        Iterable<IdItem> items = itemRepository.findAll(Sort.by("id"));
        return toSeqList(items);
    }

    private List<IdItem> insertDefaultItems() {
        List<IdItem> defaultIdItems = DEFAULT_ITEMS.stream()
            .map(this::toIncrementalIdItem)
            .collect(toList());

        return toSeqList(itemRepository.saveAll(defaultIdItems));
    }

    private IdItem toIncrementalIdItem(Item item) {
        return new IdItem(idGenerator.getAndIncrement(), item);
    }

    private static List<IdItem> toSeqList(Iterable<IdItem> items) {
        return stream(items.spliterator(), false)
            .collect(toList());
    }

    private void lockUpdates() {
        boolean locked = updating.compareAndSet(false, true);
        if (!locked) {
            throw new IllegalStateException("Another update is in progress!");
        }
    }

    private void unlockUpdates() {
        boolean completed = updating.compareAndSet(true, false);
        if (!completed) {
            throw new IllegalStateException("Updating flag was changed by some other thread");
        }
    }

}
