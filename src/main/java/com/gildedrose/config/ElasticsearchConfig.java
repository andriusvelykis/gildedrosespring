package com.gildedrose.config;

import com.gildedrose.items.ItemRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackageClasses = ItemRepository.class)
public class ElasticsearchConfig {

}
