package com.gildedrose.core;

import java.util.function.Supplier;

public class AsyncUtil {

    public static <R> R runWithLoggingAndSlowdown(boolean slowdown, Supplier<R> supplier) {
        if (!slowdown) {
            return supplier.get();
        }

        System.out.println(String.format("Computing at thread %s", Thread.currentThread().getId()));
        sleepRandomUpTo5s();
        R result = supplier.get();
        System.out.println(String.format("Finished computing at thread %s", Thread.currentThread().getId()));
        return result;
    }

    private static void sleepRandomUpTo5s() {
        try {
            long sleepTime = Math.round(Math.random() * 5000);
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
