package com.gildedrose.core;

import com.gildedrose.core.QualityRules.QualityRulesBuilder;
import com.gildedrose.items.IdItem;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import java.util.stream.Stream;

import static com.gildedrose.core.QualityRules.QualityRulesBuilder.qualityRules;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.Collections.unmodifiableMap;
import static java.util.stream.Collectors.toMap;

/**
 * The core quality/sellIn computation functionality to be reused between the array-based GildedRose class
 * and the Elasticsearch-backed GildedRoseService.
 * <p>
 * The refactoring of code into a generic fluent quality rules builder might be a bit of overkill -
 * the benefits would be more visible if there were a lot of items to specify and if the people writing the rules
 * would have less coding experience.
 * <p>
 * However, in the light of the project context (rules engine, etc), I wanted to create a generic rules builder
 * and show some Java 8 skills while doing that.
 * <p>
 * See commit 81e83de75b560b6904d78de9eb386b91d12c7f7b for a simpler version of the refactoring that uses separate
 * methods and if statements for quality calculation.
 */
public class GildedRoseCore {

    private static final int MAX_QUALITY = 50;
    private static final int MIN_QUALITY = 0;

    public static final String AGED_BRIE = "Aged Brie";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
    public static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    public static final String CONJURED = "Conjured Mana Cake";

    /**
     * The quality rule that applies to all items not otherwise specified in CUSTOM_QUALITY_RULES.
     * Would be nice to encode it together with the rest using something like
     * Scala's Map.WithDefault, commons.DefaultedMap, etc - but did not want to bring extra libs.
     * <p>
     * Use {@link #getQualityRules(String)} static method to get the rules for an item.
     */
    private static final QualityRules GENERIC_QUALITY_RULES = qualityRules()
        .byDefaultQualityChanges(q -> q - 1)
        .andWhenSellDaysLeft(0).qualityChanges(q -> q - 2).build();

    /**
     * A map recording custom quality rules defined for specific items.
     */
    // Guava's ImmutableMap would be simpler than wrapping the SimpleEntry here, but the library is huge
    private static final Map<String, QualityRules> CUSTOM_QUALITY_RULES = unmodifiableMap(Stream.of(
        entry(SULFURAS, qualityRules()
            .byDefaultQualityChanges(q -> q)),
        entry(AGED_BRIE, qualityRules()
            .byDefaultQualityChanges(q -> q + 1)
            .andAfterSellByDate().qualityChanges(q -> q + 2)),
        entry(BACKSTAGE_PASSES, qualityRules()
            .byDefaultQualityChanges(q -> q + 1)
            .andWhenSellDaysLeft(10).qualityChanges(q -> q + 2)
            .andWhenSellDaysLeft(5).qualityChanges(q -> q + 3)
            .andAfterSellByDate().qualityChanges(q -> 0)),
        entry(CONJURED, qualityRules()
            .byDefaultQualityChanges(q -> q - 2)
            .andAfterSellByDate().qualityChanges(q -> q - 4))
    ).collect(toMap(SimpleEntry::getKey, SimpleEntry::getValue)));


    private static QualityRules getQualityRules(String itemName) {
        return CUSTOM_QUALITY_RULES.getOrDefault(itemName, GENERIC_QUALITY_RULES);
    }

    public static IdItem updateItemQualityAndSellIn(IdItem idItem) {
        int newQuality = computeNewQualityWithinMinMax(idItem.name, idItem.quality, idItem.sellIn);
        int newSellIn = computeNewSellIn(idItem.name, idItem.sellIn);
        return idItem.withSellInAndQuality(newSellIn, newQuality);
    }

    private static int computeNewSellIn(String itemName, int sellIn) {
        switch (itemName) {
            case SULFURAS:
                return sellIn;
            default:
                return sellIn - 1;
        }
    }

    private static int computeNewQualityWithinMinMax(String itemName, int currentQuality, int sellIn) {
        int newQuality = computeNewQuality(itemName, currentQuality, sellIn);

        // if over max or below min already (e.g. if so initialized), use that as the bound:
        // i.e. it cannot exceed current min/max further, but can move closer to the correct range.
        // The original algorithm was implemented in this manner
        int currentMax = max(currentQuality, MAX_QUALITY);
        int currentMin = min(currentQuality, MIN_QUALITY);

        // make the new quality bounded within the [currentMin; currentMax] range
        return min(max(newQuality, currentMin), currentMax);
    }

    private static int computeNewQuality(String itemName, int currentQuality, int sellIn) {
        return getQualityRules(itemName).getNewQuality(currentQuality, sellIn);
    }

    private static SimpleEntry<String, QualityRules> entry(String itemName,
                                                           QualityRulesBuilder qualityRulesBuilder) {
        return new SimpleEntry<>(itemName, qualityRulesBuilder.build());
    }

}
