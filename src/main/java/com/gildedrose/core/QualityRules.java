package com.gildedrose.core;

import java.util.*;
import java.util.function.Function;

/**
 * A way to encode multiple quality rules based on sellIn date -
 * supported by a fluent builder {@link QualityRulesBuilder}.
 * <p>
 * A bit of overkill for GildedRose example, but showcases some Java 8 and generic rule specification skills
 */
class QualityRules {
    private final Function<Integer, Integer> defaultQualityChange;
    // need a sorted map (from lowest to highest) to retrieve the applicable quality change function
    private final SortedMap<Integer, Function<Integer, Integer>> qualityChangeBySellIn;

    QualityRules(Function<Integer, Integer> defaultQualityChange,
                 Map<Integer, Function<Integer, Integer>> qualityChangeBySellIn) {
        this.defaultQualityChange = defaultQualityChange;
        this.qualityChangeBySellIn = Collections.unmodifiableSortedMap(new TreeMap<>(qualityChangeBySellIn));
    }

    int getNewQuality(int currentQuality, int sellIn) {
        Function<Integer, Integer> matchingQualityChange = getMatchingRuleBySellIn(sellIn);
        return matchingQualityChange.apply(currentQuality);
    }

    private Function<Integer, Integer> getMatchingRuleBySellIn(int sellIn) {
        return qualityChangeBySellIn.entrySet().stream()
                // get first rule where rule key exceeds sellIn (i.e. the nearest higher sellIn rule)
                .filter(e -> sellIn <= e.getKey())
                .map(Map.Entry::getValue)
                .findFirst()
                .orElse(defaultQualityChange);
    }

    static class QualityRulesBuilder {
        private Function<Integer, Integer> defaultQualityChange;
        private final Map<Integer, Function<Integer, Integer>> qualityChangeBySellIn = new HashMap<>();

        static QualityRulesBuilder qualityRules() {
            return new QualityRulesBuilder();
        }

        QualityRulesBuilder byDefaultQualityChanges(Function<Integer, Integer> defaultQualityChange) {
            this.defaultQualityChange = defaultQualityChange;
            return this;
        }

        SellInQualityBuilder andWhenSellDaysLeft(int sellIn) {
            return new SellInQualityBuilder(sellIn);
        }

        SellInQualityBuilder andAfterSellByDate() {
            return andWhenSellDaysLeft(0);
        }

        QualityRules build() {
            return new QualityRules(defaultQualityChange, qualityChangeBySellIn);
        }

        class SellInQualityBuilder {
            private final int sellIn;

            private SellInQualityBuilder(int sellIn) {
                this.sellIn = sellIn;
            }

            QualityRulesBuilder qualityChanges(Function<Integer, Integer> qualityChange) {
                QualityRulesBuilder.this.qualityChangeBySellIn.put(sellIn, qualityChange);
                return QualityRulesBuilder.this;
            }
        }

    }
}
