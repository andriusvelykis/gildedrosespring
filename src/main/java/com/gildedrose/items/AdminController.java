package com.gildedrose.items;

import com.gildedrose.GildedRoseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for 'admin' functions (no security implemented)
 */
@RestController
public class AdminController {

    private final GildedRoseService gildedRoseService;

    @Autowired
    public AdminController(GildedRoseService gildedRoseService) {
        this.gildedRoseService = gildedRoseService;
    }

    @PostMapping("/triggerUpdateAll")
    public String updateAll() {
        gildedRoseService.updateAllAsync();
        return "Asynchronous update of items triggered";
    }

    @PostMapping("/resetItems")
    public String resetItems() {
        gildedRoseService.resetAllItems();
        return "All items have been reset";
    }

}
