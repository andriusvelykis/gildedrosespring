package com.gildedrose.items;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gildedrose.Item;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Objects;

/**
 * An immutable counterpart of the Item to use in async computations.
 * <p>
 * Also all extra annotations are placed on it.
 */
@Document(indexName = "gildedrose", type = "items")
public class IdItem {

    @Id
    public final long id;
    public final String name;
    public final int sellIn;
    public final int quality;

    @JsonCreator
    public IdItem(@JsonProperty("id") long id,
                  @JsonProperty("name") String name,
                  @JsonProperty("sellIn") int sellIn,
                  @JsonProperty("quality") int quality) {
        this.id = id;
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }

    public IdItem(long id, Item item) {
        this(id, item.name, item.sellIn, item.quality);
    }

    public IdItem withSellInAndQuality(int sellIn, int quality) {
        return new IdItem(this.id, this.name, sellIn, quality);
    }

    public void updateValuesIn(Item item) {
        item.name = name;
        item.sellIn = sellIn;
        item.quality = quality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdItem idItem = (IdItem) o;
        return id == idItem.id &&
            sellIn == idItem.sellIn &&
            quality == idItem.quality &&
            Objects.equals(name, idItem.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, sellIn, quality);
    }

    @Override
    public String toString() {
        return "IdItem{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", sellIn=" + sellIn +
            ", quality=" + quality +
            '}';
    }
}
