package com.gildedrose.items;

import com.gildedrose.GildedRoseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ItemsController {

    private final GildedRoseService gildedRoseService;

    @Autowired
    public ItemsController(GildedRoseService gildedRoseService) {
        this.gildedRoseService = gildedRoseService;
    }

    @GetMapping("/items")
    public List<IdItem> listItems() {
        return gildedRoseService.getAllItems();
    }

}
