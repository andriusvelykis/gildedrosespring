package com.gildedrose;

import com.gildedrose.items.IdItem;
import com.gildedrose.items.ItemRepository;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class GildedRoseServiceTest {

    private static final int TIMEOUT = 10000;
    private final ItemRepository repository = mock(ItemRepository.class);

    private final GildedRoseService target = new GildedRoseService(repository);

    @Test
    public void asyncUpdateSuccessful() {

        given(repository.findAll())
            .willReturn(testItems());

        // when
        target.updateAllAsync();

        verifyFirstUpdateSuccessful();

        verify(repository).findAll();
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void cannotUpdateWhileAnotherInProgress_ButCanAfterwards() throws Exception {
        given(repository.findAll())
            .willReturn(testItems());

        // block all saves on this future to prevent the first async call from completing too fast
        CompletableFuture<Void> blockUntilAfterTest = blockOnRepositorySave();

        // when: 1st update
        target.updateAllAsync();

        // when: 2nd update - should fail with exception
        assertThatThrownBy(target::updateAllAsync)
            .hasMessageContaining("Another update is in progress")
            .isInstanceOf(IllegalStateException.class);

        // given: release the block for the 1st update, verify results and reset
        blockUntilAfterTest.complete(null);

        List<IdItem> resultItems = verifyFirstUpdateSuccessful();

        // use new values for findAll for the next step to be more realistic
        given(repository.findAll()).willReturn(resultItems);
        // save without blocking
        given(repository.save(any())).will(save -> null);

        // when: 3rd update - should be ok but wait a bit in case the first one still need to finish
        // (may produce intermittent failures)
        Thread.sleep(1000);
        target.updateAllAsync();

        verify(repository, timeout(TIMEOUT)).save(new IdItem(1, "+5 Dexterity Vest", 8, 18));
        verify(repository, timeout(TIMEOUT)).save(new IdItem(2, "Aged Brie", 0, 2));

        verify(repository, times(2)).findAll();
        verifyNoMoreInteractions(repository);
    }

    private List<IdItem> verifyFirstUpdateSuccessful() {
        IdItem vestAfter1Update = new IdItem(1, "+5 Dexterity Vest", 9, 19);
        IdItem brieAfter1Update = new IdItem(2, "Aged Brie", 1, 1);
        verify(repository, timeout(TIMEOUT)).save(vestAfter1Update);
        verify(repository, timeout(TIMEOUT)).save(brieAfter1Update);
        return Arrays.asList(vestAfter1Update, brieAfter1Update);
    }

    private CompletableFuture<Void> blockOnRepositorySave() {
        CompletableFuture<Void> blockUntilAfterTest = new CompletableFuture<>();

        doAnswer((param) -> {
            blockUntilAfterTest.join();
            return null;
        }).when(repository).save(any());
        return blockUntilAfterTest;
    }

    private List<IdItem> testItems() {
        return Arrays.asList(
            new IdItem(1, "+5 Dexterity Vest", 10, 20),
            new IdItem(2, "Aged Brie", 2, 0)
        );
    }

}
