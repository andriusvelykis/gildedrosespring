package com.gildedrose;

import com.gildedrose.core.GildedRoseCore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static com.gildedrose.GildedRoseTest.SingleItemTestBuilder.forItem;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class GildedRoseTest {

    private static final String GENERIC_NAME = "foo";
    private static final String AGED_BRIE = GildedRoseCore.AGED_BRIE;
    private static final String SULFURAS = GildedRoseCore.SULFURAS;
    private static final String BACKSTAGE_PASSES = GildedRoseCore.BACKSTAGE_PASSES;
    private static final String CONJURED = GildedRoseCore.CONJURED;

    private GildedRose app;

    @Parameters(name = "{index}: {0}")
    public static Collection<Object[]> testTable() {
        return Arrays.asList(
            forItem(GENERIC_NAME).sellInChange(20, 19).qualityChange(30, 29).build("both sell in and quality decrease by 1"),
            forItem(GENERIC_NAME).sellInChange(1, 0).qualityChange(30, 29).build("both sell in and quality decrease by 1 before sell-by date"),
            forItem(GENERIC_NAME).sellInChange(0, -1).qualityChange(30, 28).build("quality decreases by 2 at sell-by date"),
            forItem(GENERIC_NAME).sellInChange(-1, -2).qualityChange(30, 28).build("quality decreases by 2 after sell-by date"),
            forItem(GENERIC_NAME).sellInChange(20, 19).qualityChange(0, 0).build("quality does not become negative"),
            forItem(GENERIC_NAME).sellInChange(-1, -2).qualityChange(0, 0).build("quality does not become negative after sell-by date"),
            forItem(GENERIC_NAME).sellInChange(-1, -2).qualityChange(1, 0).build("quality does not become negative (but decreases to 0) after sell-by date"),
            forItem(GENERIC_NAME).sellInChange(20, 19).qualityChange(-10, -10).build("quality stays negative if initialized"),
            forItem(GENERIC_NAME).sellInChange(-1, -2).qualityChange(-10, -10).build("quality stays negative past sell-by date if initialized"),
            forItem(GENERIC_NAME).sellInChange(20, 19).qualityChange(60, 59).build("quality decreases by 1 when over 50"),
            forItem(GENERIC_NAME).sellInChange(-1, -2).qualityChange(60, 58).build("quality decreases by 2 when over 50"),
            forItem(AGED_BRIE).sellInChange(20, 19).qualityChange(10, 11).build("Aged Brie quality increases by 1"),
            forItem(AGED_BRIE).sellInChange(0, -1).qualityChange(10, 12).build("Aged Brie quality increases by 2 on sell-by date"),
            forItem(AGED_BRIE).sellInChange(-1, -2).qualityChange(10, 12).build("Aged Brie quality increases by 2 after sell-by date"),
            forItem(AGED_BRIE).sellInChange(20, 19).qualityChange(-10, -9).build("Aged Brie quality increases by 1 from negative"),
            forItem(AGED_BRIE).sellInChange(-1, -2).qualityChange(-10, -8).build("Aged Brie quality increases by 2 from negative"),
            forItem(AGED_BRIE).sellInChange(-1, -2).qualityChange(-1, 1).build("Aged Brie quality increases by 2 and becomes positive"),
            forItem(AGED_BRIE).sellInChange(20, 19).qualityChange(50, 50).build("Aged Brie quality never more than 50"),
            forItem(AGED_BRIE).sellInChange(-1, -2).qualityChange(49, 50).build("Aged Brie quality never more than 50 (but increases to 50)"),
            forItem(AGED_BRIE).sellInChange(20, 19).qualityChange(60, 60).build("Aged Brie quality does not increase when over 50"),
            forItem(SULFURAS).sellInChange(20, 20).qualityChange(10, 10).build("Sulfuras sellIn and quality do not change"),
            forItem(SULFURAS).sellInChange(0, 0).qualityChange(10, 10).build("Sulfuras sellIn and quality do not change on sell-by date"),
            forItem(SULFURAS).sellInChange(-1, -1).qualityChange(10, 10).build("Sulfuras sellIn and quality do not change after sell-by date"),
            forItem(SULFURAS).sellInChange(20, 20).qualityChange(-10, -10).build("Sulfuras quality does not change when negative"),
            forItem(SULFURAS).sellInChange(20, 20).qualityChange(80, 80).build("Sulfuras quality does not change when over 50"),
            forItem(BACKSTAGE_PASSES).sellInChange(20, 19).qualityChange(10, 11).build("Backstage passes quality increases by 1"),
            forItem(BACKSTAGE_PASSES).sellInChange(11, 10).qualityChange(10, 11).build("Backstage passes quality increases by 1 at 11 days"),
            forItem(BACKSTAGE_PASSES).sellInChange(10, 9).qualityChange(10, 12).build("Backstage passes quality increases by 2 when 10 days or less"),
            forItem(BACKSTAGE_PASSES).sellInChange(6, 5).qualityChange(10, 12).build("Backstage passes quality increases by 2 at 6 days"),
            forItem(BACKSTAGE_PASSES).sellInChange(5, 4).qualityChange(10, 13).build("Backstage passes quality increases by 3 when 5 days or less"),
            forItem(BACKSTAGE_PASSES).sellInChange(1, 0).qualityChange(10, 13).build("Backstage passes quality increases by 3 at 1 day"),
            forItem(BACKSTAGE_PASSES).sellInChange(0, -1).qualityChange(10, 0).build("Backstage passes quality becomes 0 on sell-by date"),
            forItem(BACKSTAGE_PASSES).sellInChange(-1, -2).qualityChange(10, 0).build("Backstage passes quality becomes 0 after sell-by date"),
            forItem(BACKSTAGE_PASSES).sellInChange(20, 19).qualityChange(50, 50).build("Backstage passes quality never more than 50"),
            forItem(BACKSTAGE_PASSES).sellInChange(10, 9).qualityChange(49, 50).build("Backstage passes quality never more than 50 (but increases to 50 by 2)"),
            forItem(BACKSTAGE_PASSES).sellInChange(5, 4).qualityChange(48, 50).build("Backstage passes quality never more than 50 (but increases to 50 by 3)"),
            forItem(BACKSTAGE_PASSES).sellInChange(20, 19).qualityChange(-10, -9).build("Backstage passes quality increases by 1 from negative"),
            forItem(BACKSTAGE_PASSES).sellInChange(10, 9).qualityChange(-10, -8).build("Backstage passes quality increases by 2 from negative"),
            forItem(BACKSTAGE_PASSES).sellInChange(5, 4).qualityChange(-10, -7).build("Backstage passes quality increases by 2 from negative"),
            forItem(BACKSTAGE_PASSES).sellInChange(5, 4).qualityChange(-2, 1).build("Backstage passes quality becomes positive"),
            forItem(BACKSTAGE_PASSES).sellInChange(-1, -2).qualityChange(-10, 0).build("Backstage passes quality becomes 0 from negative"),
            forItem(BACKSTAGE_PASSES).sellInChange(-1, -2).qualityChange(60, 0).build("Backstage passes quality becomes 0 from over 50"),
            forItem(BACKSTAGE_PASSES).sellInChange(20, 19).qualityChange(60, 60).build("Backstage passes quality does not increase when over 50"),
            forItem(CONJURED).sellInChange(20, 19).qualityChange(30, 28).build("conjured quality decreases by 2"),
            forItem(CONJURED).sellInChange(0, -1).qualityChange(30, 26).build("conjured quality decreases by 4 at sell-by date"),
            forItem(CONJURED).sellInChange(-1, -2).qualityChange(30, 26).build("conjured quality decreases by 4 after sell-by date"),
            forItem(CONJURED).sellInChange(20, 19).qualityChange(0, 0).build("conjured quality does not become negative"),
            forItem(CONJURED).sellInChange(-1, -2).qualityChange(0, 0).build("conjured quality does not become negative after sell-by date"),
            forItem(CONJURED).sellInChange(20, 19).qualityChange(1, 0).build("conjured quality does not become negative (but decreases to 0)"),
            forItem(CONJURED).sellInChange(-1, -2).qualityChange(3, 0).build("conjured quality does not become negative (but decreases to 0) after sell-by date"),
            forItem(CONJURED).sellInChange(20, 19).qualityChange(-10, -10).build("conjured quality stays negative if initialized"),
            forItem(CONJURED).sellInChange(-1, -2).qualityChange(-10, -10).build("conjured quality stays negative past sell-by date if initialized"),
            forItem(CONJURED).sellInChange(20, 19).qualityChange(60, 58).build("conjured quality decreases by 2 when over 50"),
            forItem(CONJURED).sellInChange(-1, -2).qualityChange(60, 56).build("conjured quality decreases by 4 when over 50")
        );
    }

    @Parameter
    public String comment;

    @Parameter(1)
    public String itemName;

    @Parameter(2)
    public int sellIn;

    @Parameter(3)
    public int quality;

    @Parameter(4)
    public int expectedSellIn;

    @Parameter(5)
    public int expectedQuality;

    @Test
    public void testSingleItem() {
        givenSingleItem(itemName, sellIn, quality);
        whenUpdateQuality();
        thenFirstItemIs(itemName, expectedSellIn, expectedQuality);
    }


    private void givenSingleItem(String name, int sellIn, int quality) {
        Item[] items = new Item[]{new Item(name, sellIn, quality)};
        this.app = new GildedRose(items);
    }

    private void whenUpdateQuality() {
        app.updateQuality();
    }

    private void thenFirstItemIs(String expectedName, int expectedSellIn, int expectedQuality) {
        Item item = app.items[0];
        assertThat(item.name).as(comment + ": name does not match").isEqualTo(expectedName);
        assertThat(item.sellIn).as(comment + ": sellIn does not match").isEqualTo(expectedSellIn);
        assertThat(item.quality).as(comment + ": quality does not match").isEqualTo(expectedQuality);
    }

    /**
     * A fluent builder to construct test case parameters
     */
    static class SingleItemTestBuilder {
        private String itemName;
        private Integer sellIn;
        private Integer quality;
        private Integer expectedSellIn;
        private Integer expectedQuality;

        static SingleItemTestBuilder forItem(String itemName) {
            SingleItemTestBuilder builder = new SingleItemTestBuilder();
            builder.itemName = itemName;
            return builder;
        }

        private SingleItemTestBuilder sellInChange(int sellIn, int expectedSellIn) {
            this.sellIn = sellIn;
            this.expectedSellIn = expectedSellIn;
            return this;
        }

        private SingleItemTestBuilder qualityChange(int quality, int expectedQuality) {
            this.quality = quality;
            this.expectedQuality = expectedQuality;
            return this;
        }

        private Object[] build(String comment) {
            return new Object[]{comment, itemName, sellIn, quality, expectedSellIn, expectedQuality};
        }

    }

}
