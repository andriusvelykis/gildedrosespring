package com.gildedrose.items;

import com.gildedrose.GildedRoseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ItemsController.class)
public class ItemsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GildedRoseService gildedRoseService;

    @Test
    public void itemsRetrieved() throws Exception {
        given(gildedRoseService.getAllItems())
            .willReturn(testItems());

        String expectedItemsJson = loadFileContents("expectedItems.json");

        mockMvc.perform(get("/items").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().json(expectedItemsJson));
    }

    private List<IdItem> testItems() {
        return Arrays.asList(
            new IdItem(1, "FooItem", -1, 500),
            new IdItem(500, "Some other item", 20, 0)
        );
    }

    private String loadFileContents(String path) throws IOException {
        return StreamUtils.copyToString(
            new ClassPathResource(path).getInputStream(),
            Charset.defaultCharset());
    }

}
